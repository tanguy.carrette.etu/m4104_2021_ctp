package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    private String[] liste_Salle;
    // TODO Q2.c
    SuiviViewModel viewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // TODO Q1
        liste_Salle=getContext().getResources().getStringArray(R.array.list_salles);
        DISTANCIEL= liste_Salle[0];
        // TODO Q2.c
        viewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4

        //SALLE
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapterS = ArrayAdapter.createFromResource(super.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapterS);

        //POSTES
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterP = ArrayAdapter.createFromResource(super.getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterP);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView loging = (TextView) view.findViewById(R.id.tvLogin);
            viewModel.setUsername(loging.getText()+"");

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                poste = parent.getItemAtPosition(pos).toString();
                update();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                salle = parent.getItemAtPosition(pos).toString();
                update();
            }
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if(this.salle.equals("Distanciel")){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            viewModel.setLocalisation("Distanciel");
        }
        else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            viewModel.setLocalisation(this.salle + ":" + this.poste);
        }
    }
    // TODO Q9
}